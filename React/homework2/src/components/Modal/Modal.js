import React, { Component } from "react";
import "./Modal.scss";
import PropTypes from "prop-types";


class Modal extends Component {
  render() {
    const {header, text, func, actions} = this.props;
    function handleClick(e) {
      if (e.target.className === "modal-wrapper") {
        func(e);
      }
    }
    return (
      <div className = "modal-wrapper" onClick = {handleClick}>
        <div className = "modal">
          <div className = 'modal-header'>
            <h1 className = 'modal-title'>{header}</h1>
          </div>
            {text}
          <div>
            {actions}
          </div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  text: PropTypes.object.isRequired,
  func: PropTypes.func.isRequired,
  actions: PropTypes.array.isRequired,
};

export default Modal;
