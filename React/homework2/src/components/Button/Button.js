import React, { Component } from 'react';
import './Button.scss';
import PropTypes from "prop-types";


class Button extends Component {
  render() {
    const {id, className, text, backgroundColor, func} =this.props
    return (
      <button id = {id} className = {className} style = {{backgroundColor}} onClick = {func} >{text}</button>
    );
  }
}

Button.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string.isRequired,
  func: PropTypes.func,
};

Button.defaultProps = {
  id: "noneID",
  func: null,
};

export default Button;
