import React, { Component } from "react";
import "./Card.scss";
import Button from "../Button/Button";
import Icon from "../Icon/Icon";
import PropTypes from "prop-types";

class Card extends Component {
  render() {
    const { card, func, icon, fav } = this.props;

    return (
      <div className="product-card">
        <h3>{card.title}</h3>
        <img src={card.imgUrl} style={{ width: 150, height: 80}} alt={card.title}></img>
        <p className="product-article">Article: {card.article}</p>
        <p>Price: {card.price}$</p>
        <p>Color: {card.color}</p>
        <div>
          <Button
            id={card.article}
            text="Add to cart"
            className="card-button"
            func={func}
          />
          {icon ? (
            <Icon id={card.article} className="favor-icon" func={fav} />
          ) : null}
        </div>
      </div>
    );
  }
}

Card.propTypes = {
  card: PropTypes.object.isRequired,
  icon: PropTypes.bool,
  fav: PropTypes.func,
  func: PropTypes.func,
};
Card.defaultProps = {
  card: null,
};

export default Card;
