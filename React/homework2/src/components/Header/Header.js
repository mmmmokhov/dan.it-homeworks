import React from 'react';
import './Header.scss';
import logo from '../../logo.svg';

const Header = () => {
  return (
    <div>
      <div>
        <img className="header-logo" src={logo} alt="MERCEDES logo" />
      </div>
    </div>
  )
}

export default Header;
