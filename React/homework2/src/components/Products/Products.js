import React, { Component } from "react";
import PropTypes from "prop-types";
import Card from "../Card/Card";
import "./Products.scss";

class Products extends Component {
  render() {
    const { products, func, fav } = this.props;
    const prodCards = products.map((e) => (
      <Card key={e.article} card={e} fav={fav} func={func} icon={true} />
    ));
    return <div className="products-wrapper">{prodCards}</div>;
  }
}

Products.propTypes = {
  products: PropTypes.arrayOf(
    PropTypes.shape({
      article: PropTypes.string.isRequired,
    })
  ).isRequired,
  func: PropTypes.func,
  fav: PropTypes.func,
};
Products.defaultProps = {
  func: null,
  fav: null,
};

export default Products;
