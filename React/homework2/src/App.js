import React, { Component } from "react";
import "./App.css";
import axios from "axios";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import Products from "./components/Products/Products";
import Header from "./components/Header/Header";


class App extends Component {
  state = {
    cards: [],
    showModal: false,
  };

  // closeBtn = () => {
  //   this.setState(
  //     (state) =>
  //       (state = {
  //         ...state,
  //         causedModal: null,
  //       })
  //   );
  // };

  // handleClick = (e) => {
  //   return this.setState(
  //     (state) =>
  //       (state = {
  //         ...state,
  //         causedModal: e.target.id
  //       })
  //   );
  // };

  componentDidMount() {
    axios("/cars-catalogue.json").then((res) => {
      this.setState({ cards: res.data });
    });
    if (!localStorage.getItem("cart")) localStorage.setItem("cart", "[]");
    if (!localStorage.getItem("favorites"))
      localStorage.setItem("favorites", "[]");
  }

  showModal = (ev) => {
    const check = !this.state.showModal;
    this.setState({
      ...this.state,
      showModal: check,
    });
    this.targetProd = ev.target.id;
  };

  addToCart = () => {
    let cartProds = JSON.parse(localStorage.getItem("cart"));
    let newProds = cartProds.map((el) => el);
    newProds.push(this.targetProd);
    localStorage.setItem("cart", JSON.stringify(newProds));
    this.setState({
      ...this.state,
      showModal: false,
    });
  };

  addToFav = (e) => {
    let favProds = JSON.parse(localStorage.getItem("favorites"));
    let newProds = favProds.map((el) => el);
    if (!newProds.includes(e.target.parentElement.id)) {
      newProds.push(e.target.parentElement.id);
      localStorage.setItem("favorites", JSON.stringify(newProds));
      e.target.classList.toggle("active");
    } else if (newProds.includes(e.target.parentElement.id)) {
      let index = newProds.indexOf(e.target.parentElement.id);
      newProds.splice(index, 1);
      localStorage.setItem("favorites", JSON.stringify(newProds));
      e.target.classList.toggle("active");
    }
  };

  actions = [
    <Button
      className="yes-btn"
      key="yesbtn"
      text="YES"
      func={this.addToCart}
    />,
    <Button
      className="no-btn"
      key="nobtn"
      text="NO"
      func={() =>
        this.setState({
          ...this.state,
          showModal: false
        })
      }
    />,
  ];

  render() {
    const { cards, showModal } = this.state;
    const actions = this.actions;
    return (
      <div className="App">
        <Header />
        <Products products={cards} func={this.showModal} fav={this.addToFav} />
        {showModal ? (
          <Modal
            func={this.showModal}
            header={"Are you ready to buy this SUPERCAR?"}
            text={
              <p className="modal-text">
                The machine will be added to the cart. If one is not enough for you, you can continue shopping :)
              </p>
            }
            actions={actions}
          />
        ) : null}
      </div>
    );
  }
}

export default App;
