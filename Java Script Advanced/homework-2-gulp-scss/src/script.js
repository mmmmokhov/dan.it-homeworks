function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show"),
    document.getElementById("myDropdownBtn").classList.toggle("hero-nav__dropbtn_active")
}

window.onclick = function (t) {
    if (!t.target.matches(".hero-nav__dropbtn") && !t.target.matches(".btn-line"))
    for (var n=document.getElementsByClassName("hero-nav__dropdown-content"), e=0; e < n.length; e++) {
        var o=n[e]; o.classList.contains("show") && o.classList.remove("show")
    }
};

const menuLinks = document.querySelectorAll('.hero-nav__link[data-goto]');
if (menuLinks.length > 0) {
    menuLinks.forEach(menuLink => {
        menuLink.addEventListener("click", onMenuLinkClick);
    });

    function onMenuLinkClick(e) {
        const menuLink = e.target;
        if (menuLink.dataset.goto && document.querySelector(menuLink.dataset.goto)) {
            const gotoBlock = document.querySelector(menuLink.dataset.goto);
            const gotoBlockValue = gotoBlock.getBoundingClientRect().top + pageYOffset - document.querySelector('header').offsetHeight;

            window.scrollTo({
                top: gotoBlockValue,
                behavior: "smooth"
            });
            e.preventDefault();
        }
    }
}
