const url = "https://ajax.test-danit.com/api/swapi/",
  heads = { "Content-type": "application/json" };
const films = fetch(`${url}films`, {
  method: "get",
  headers: heads,
});

filmsList = document.createElement("div");
filmsList.className = "film";
document.getElementById("root").append(filmsList);
films
  .then((resp) => {
    if (resp.status === 200) {
      return resp.json();
    }
  })
  .then((films) => {
    films.forEach((film) => {
      const ef = new Film(film);
      ef.render(filmsList);
    });
  })
  .catch((e) => console.error(e));
