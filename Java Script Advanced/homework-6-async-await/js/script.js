function main() {
  let btn = document.createElement("button"),
    parent = document.getElementById("data"),
    catchIp = new GetIp();
  btn.className = "btn";
  btn.innerText = "Вычислить по IP";

  btn.addEventListener("click", async () => {
    try {
      await catchIp.getIp();
      catchIp.render(parent);
    } catch (e) {
      console.error(e);
    }
  });

  parent.insertAdjacentElement("afterbegin", btn);
}

class GetIp {
  options = {
    method: "get",
    headers: { "Content-type": "application/json" },
  };
  elements = {
    list: document.createElement("ul"),
  };
  resultInfo = {};
  async getIp() {
    let wrapper = await this.makeRequest(
      "https://api.ipify.org/?format=json",
      this.options
    );

    if (!wrapper["ip"]) {
      throw new Error("Ошибка при получении данных IP!");
    }

    this.resultInfo = await this.makeRequest(
      `http://ip-api.com/json/${wrapper.ip}`,
      this.options
    );
    if (Object.keys(this.resultInfo).length <= 0) {
      throw new Error("Нет данных!");
    }
  }

  async makeRequest(uri, options) {
    let response = await fetch(uri, options);
    let wrapper = await response.json();
    return wrapper;
  }

  render(parent) {
    let { list } = this.elements;
    for (let key in this.resultInfo) {
      let line = document.createElement("li");
      line.innerText = `${key}: ${this.resultInfo[key]};`;
      list.append(line);
    }
    parent.insertAdjacentElement("beforeend", list);
  }
}

main();
