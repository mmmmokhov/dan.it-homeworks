function createNewUser(firstName, lastName) {
  const newUser = {
    firstName: prompt("enter Name", "Oleksandr"),
    lastName: prompt("enter lastName", "Mokhov"),
    getLogin() {
      return (this.firstName[0] + this.lastName).toLowerCase()
    },
    setFirstName(newFirstName) {
      this.firstName = newFirstName
    },
    setLastName(newLastName) {
      this.lastName = newLastName
    }
  }
  Object.defineProperty(newUser, 'firstName', {
    writable: false
  })
  Object.defineProperty(newUser, 'lastName', {
    writable: false
  })

  return newUser
}

const newUser = createNewUser()
console.log(newUser.getLogin())
