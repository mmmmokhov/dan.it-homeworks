let userNumber = +prompt("Please enter number");
while (isNaN(userNumber) || userNumber == "" || !Number.isInteger(userNumber)) {
    userNumber = +prompt("Enter correct number")
};

const m = 5

if (userNumber >= m) {
    for (let i = 0; i <= userNumber; i++) {
        if (i % m === 0 && i !== 0) {
            console.log(i);
        }
    }
} else if (userNumber <= -m) {
    for (let i = 0; i >= userNumber; i--) {
        if (i % m === 0 && i !== 0) {
            console.log(i);
        }
    }
} else {
    console.log("Sorry, no numbers");
};


// ************ Advanced task ******************


// let m = +prompt("Please enter number");
// let n = +prompt("Please enter number");
// while (isNaN(m || n) || (m || n) == "" || !Number.isInteger(m || n)) {
//     m = +prompt("Enter correct number")
//     n = +prompt("Enter correct number")
// };

// nextPrime:
// for (let i = m; i <= n; i++) {
//     for (let j = m; j < i; j++) {
//         if (i % j == 0) continue nextPrime
//     }
//     console.log(i)
// }
