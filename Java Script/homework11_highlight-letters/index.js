const btn = document.getElementsByClassName("btn");
const btns = [...btn];

document.addEventListener("keydown", (e) => {
  btns.forEach((item) => {
    if (e.code === "Key" + item.innerText || e.key === item.innerText) {
      item.classList.add("active");
    } else {
      item.classList.remove("active");
    }
  });
});
