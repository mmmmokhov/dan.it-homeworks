function getArray(array, parent = document.body) {
  const block = document.createElement("ul");

  array.map(function (elem) {
    const listElem = document.createElement("li");

    if (Array.isArray(elem)) {
      getArray(elem, listElem);
    } else {
      listElem.innerText = `${elem}`;
    }

    block.append(listElem);
  });

  parent.append(block);
}

function timer(sec) {
  let clock = document.createElement("h2");
  clock.innerText = `Destroing after ${sec} seconds`;

  setInterval(() => {
    clock.innerText = `Destroing after ${sec - 1} seconds`;
    if (sec > 0) {
      sec = sec - 1;
    } else {
      sec = 0;
    }
  }, 1000);
  document.body.prepend(clock);
}

setTimeout(() => document.body.remove(), 3000);

timer(3);
getArray(["Kharkiv", "Kyiv", ["Borispil", "Irpin"], "Odesa", "Lviv", "Dnipro"]);
