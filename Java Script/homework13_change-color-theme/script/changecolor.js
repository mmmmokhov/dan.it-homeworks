const btn = document.querySelector(".color-theme");

let bodyColor = document.getElementsByTagName("body")[0];

if (localStorage.getItem("bgColor") !== null) {
  const color = localStorage.getItem("bgColor");
  bodyColor.style.backgroundColor = color;
}

btn.addEventListener("click", function () {
  if (bodyColor.style.backgroundColor !== "purple") {
    bodyColor.style.backgroundColor = "purple";
    btn.style.backgroundColor = "blue";
    localStorage.setItem("bgColor", "purple");
    console.log(localStorage);
  } else {
    bodyColor.style.backgroundColor = "lightblue";
    btn.style.backgroundColor = "yellow";
    localStorage.setItem("bgColor", "lightblue");
    console.log(localStorage);
  }
});
