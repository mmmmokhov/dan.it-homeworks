let tabs = document.querySelector(".tabs");
let content = [...document.querySelectorAll(".tabs-content li")];

tabs.addEventListener("click", (event) => {
  content.forEach((el) => {
    if (el.getAttribute("data-atr") === event.target.getAttribute("data-atr")) {
      el.style.display = "block";
    } else {
      el.style.display = "none";
    }
  });
});
