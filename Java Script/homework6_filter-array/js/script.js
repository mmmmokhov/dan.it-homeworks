const arr = ['hello', 'world', 23, '23', null, 0, '32pe', {}, false, function(){}];
const type = prompt('Enter type of date (string, number, bigint, boolean, string, symbol, object, function)');

function filterBy(array, value) {
  return array.filter(function (item) {
    return typeof item !== value;
  });
}

console.log(filterBy(arr, type));
