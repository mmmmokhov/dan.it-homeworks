let slides = document.querySelectorAll(".image-to-show");
let currentSlide = 0;
let slideInterval = setInterval(nextSlide, 3000);
slides[currentSlide].className = "image-to-show-active";

let wrapper = document.querySelector(".images-wrapper");

wrapper.insertAdjacentHTML(
  "beforeend",
  `<div class = "btns-wrapper">
        <button class="stop btn" style="cursor:pointer">Прекратить</button>
        <button class = "continue btn" style="cursor:pointer">Возобновить показ</button>
    </div>`
);

function nextSlide() {
  if (slides[currentSlide].classList.contains("image-to-show")) {
    slides[currentSlide].className = "image-to-show-active";
  } else if (slides[currentSlide].classList.contains("image-to-show-active")) {
    slides[currentSlide].className = "image-to-show";
    currentSlide = ++currentSlide % slides.length;
    slides[currentSlide].className = "image-to-show-active";
  }
  timer(3);
}

document.querySelector(".stop").addEventListener("click", () => {
    clearInterval(slideInterval);
});

document.querySelector(".continue").addEventListener("click", () => {
    if (slideInterval) {
        clearInterval(slideInterval);
    }
    slideInterval = setInterval(nextSlide, 3000);
});

function timer(sec) {
    let clock = document.createElement("h2");
    clock.innerText = `Change slide at ${sec} seconds`;

    setInterval(() => {
        clock.innerText = `Change slide at ${sec - 1} seconds`;
        if (sec > 1) {
            sec = sec - 1;
        } else {
            clock.remove();
        }
    }, 1000);
    document.body.prepend(clock);
}
