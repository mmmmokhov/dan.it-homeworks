const icons = document.getElementsByClassName("icon-password");
iconsArr = [...icons];

const submitBtn = document.querySelector(".btn");
const input = document.getElementById("enter-password");
const checkInput = document.getElementById("check-password");
const passwordForm = document.querySelector(".password-form");

iconsArr.forEach((item) => {
  item.addEventListener("click", (e) => {
    const icon = e.target;

    if (icon.classList.value.includes("fa-eye-slash")) {
      icon.classList.remove("fa-eye-slash");
      icon.classList.add("fa-eye");

      if (e.target.id === "icon-input") {
        input.type = "password";
      } else {
        checkInput.type = "password";
      }
    } else {
      icon.classList.remove("fa-eye");
      icon.classList.add("fa-eye-slash");

      if (e.target.id === "icon-input") {
        input.type = "text";
      } else {
        checkInput.type = "text";
      }
    }
  });
});

const errorText = document.createElement("p");
errorText.innerText = "Нужно ввести одинаковые значения";
errorText.classList.add("error-text");
errorText.classList.add("is-hidden");
submitBtn.before(errorText);

submitBtn.addEventListener("click", (e) => {
  e.preventDefault();

  if (input.value === checkInput.value) {
    errorText.classList.add("is-hidden");
    alert("You are welcome");
  } else {
    errorText.classList.remove("is-hidden");
  }
});
