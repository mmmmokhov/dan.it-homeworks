let num1 = prompt("Enter first number");

while (isNaN(num1)) {
    num1 = prompt("Wrong data! Enter again!", num1);
}

let num2 = prompt("Enter second number");

while (isNaN(num2)) {
    num2 = prompt("Wrong data! Enter again!", num2);
}

let operation = prompt("Enter operation");
while (operation !== '+' && operation !== '-' && operation !== '*' && operation !== '/') {
    operation = prompt("Enter correct operation!");
}

function mathOp(operation) {
    switch (operation) {
        case '+':
            return (+num1 + +num2);
        case '-':
            return (num1 - num2);
        case '*':
            return (num1 * num2);
        case '/':
            return (num1 / num2);
    }
}

console.log(mathOp(operation));
