const container = document.querySelector(".root");

container.insertAdjacentHTML(
  "afterbegin",
  `<div id="form">
        Price
        <input id="input" type="number" placeholder="Price"></input>
    </div>`
);

function focus() {
  let inputPlace = document.getElementById("input");
  inputPlace.style.borderWidth = "thick";
  let warning = document.getElementById("form");
  inputPlace.onfocus = function () {
    inputPlace.style.borderColor = "green";
    document.querySelector("span").remove();
  };

  inputPlace.onblur = function () {
    inputPlace.style.borderColor = "#000";
    if (inputPlace.value >= 0 && inputPlace.value !== "") {
      inputPlace.style.color = "green";
      warning.insertAdjacentHTML(
        "afterbegin",
        `<span id="curentPrice">Текущая цена: ${inputPlace.value}<button id = "cancelPrice">x</button></span>`
      );
    } else {
      inputPlace.style.color = "red";
      warning.insertAdjacentHTML(
        "afterend",
        `<span id='errorPrice'>Please enter correct price<button id = "cancelPrice">x</button></span>`
      );
    }

    let cancelPrice = document.getElementById("cancelPrice");
    cancelPrice.addEventListener("click", () => {
      cancelPrice.parentNode.remove();
      inputPlace.value = "";
    });
  };
}

focus();
